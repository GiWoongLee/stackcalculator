package stackCalculator;

public class Stack {
	
	
	class Node{
		Node prev;
		String item;
		Node next;
	
		public Node(String str){
			prev = null;
			item = str;
			next = null;
		}
	}

	Node top;
	Node pointer;
	
	public Stack(){
		top = null;
		pointer=null;
	}
	
	public boolean isEmpty(){
		if(top==null)
			return true;
		else
			return false;
	}
	
	public void push(String str){
		Node newly = new Node(str);
		if(isEmpty()){
			top = newly;
		}
		else{
			newly.next = top;
			top.prev=newly;
			top = newly;
		}
	}
	
	public String pop(){
		if(isEmpty())
			return null;
		else{
			String temp = top.item;
			return temp;
		}
			
	}
	
	public String remove(){
		if(isEmpty())
			return null;
		else if(top.next==null){
			String temp = top.item;
			top =null;
			return temp;
		}
		else{
			String temp = top.item;
			top.next.prev=null;
			top = top.next;
			return temp;
		}
	}
	
	public void display(){
		if(isEmpty())
			return;
		else{
			Node last = top;
			while(last.next!=null)
				last = last.next;
			while(true){
				String temp = last.item;
				if(last==top){
					System.out.print(temp);
					break;
				}
				else{
					System.out.print(temp + " ");
				}
				last=last.prev;
			}
			System.out.println("");
		}
	}
	
	public void atLast(){
		pointer = top;
		while(pointer.next!=null)
			pointer = pointer.next;
	}
	

	public boolean existBracket(){
		if(isEmpty())
			return false;
		else{
			Node last = top;
			while(last.next!=null)
				last = last.next;
			while(true){
				String temp = last.item;
				char tempItem = temp.charAt(0);
				if(tempItem=='(')
					return true;
				if(last==top)
					break;
				last=last.prev;
			}
			return false;
		}
	}
	
	public boolean toomuchOperator(){
		if(isEmpty())
			return true;
		else{
			Node last = top;
			int operator = 0;
			int operand = 0;
			while(last.next!=null)
				last = last.next;
			while(true){
				String temp = last.item;
				char tempItem = temp.charAt(0);
				if(Character.isDigit(tempItem))
					operand += 1;
				else if(tempItem == '^' || tempItem == '-' || tempItem == '*' || tempItem == '/' || tempItem== '%' || tempItem=='+')
					operator += 1;
				if(last==top)
					break;
				last=last.prev;
			}
			
			if(operand==operator+1)
				return false;
			else
				return true;
		}
	}
	
}
