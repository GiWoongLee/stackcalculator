package stackCalculator;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;

public class CalculatorTest
{
	public static void main(String args[])
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		while (true)
		{
			try
			{
				String input = br.readLine();
				if (input.compareTo("q") == 0)
					break;

				command(input);
			}
			catch (Exception e)
			{
				System.out.println("ERROR");
			}
		}
	}

	private static void command(String input) throws Exception{
		Stack postfix = new Stack();
		//postfix로 변환하기
		postfix = conversion(input);
		//계산하여 값 출력하기
		String result = calculation(postfix);
		//postfix 형태 출력
		postfix.display();
		//계산 값 출력
		System.out.println(result);
	}
		
	private static Stack conversion(String infix) throws Exception{
		Stack postfix = new Stack();
		Stack operator = new Stack();
		//STEP1 : string 각각의 char를 operator/operand/whitespace로 구분해서 stack에 push
		//평상 시에는 string을 1자리씩 탐색하나, operand가 긴 경우에는 adder값의 변화를 줘서 다음 operator/operand로 이동
		for(int index=0,adder=0;index<infix.length();index=index+adder){
			//CASE1 : whitespace제거
			if(infix.charAt(index)== ' '){
				adder=1;
				continue;
			}
			
			//CASE2 : digit은 stack에 넣기
			int digitNumber = checkDigit(infix,index);
			if(digitNumber>0){
				adder=digitNumber;
				String substring = infix.substring(index, index + digitNumber);
				postfix.push(substring);
				continue;
			}
			
			//CASE3 : operator은 postfix, operator stack 두 군데를 오고가며 적절하게 집어넣기
			if(isOperator(infix.charAt(index))){
				char op = infix.charAt(index);
				String OP = Character.toString(op);
				
				//에러처리 : () error
				if(op=='('){
					if(infix.charAt(index+1)==')')
						throw new Exception();
				}
				
				if(operator.isEmpty()){
					//예외처리 : '-'부호, ex)-3
					if(postfix.isEmpty()&&op=='-')
						OP = Character.toString('~');
					operator.push(OP);
				}
				
				else{
					
					//예외처리 -> )의 경우에는 반대 (가 나올때까지 operator stack에서 item을 pop
					if(op==')'){
						String BEFOREOP = operator.remove();
						char beforeOP = BEFOREOP.charAt(0);
						adder=1;
						if(beforeOP=='(')
							continue;
						do{
							postfix.push(BEFOREOP);
							BEFOREOP = operator.remove();
							beforeOP = BEFOREOP.charAt(0);
						}while(beforeOP!='(');
						continue;
					}
					//input operator와 OPERATOR STACK에 위치한 operator들을 비교하며 집어넣기
					else
						shiftingOperator(postfix,operator, OP);
				adder=1;
				}
				
				//예외처리 : '-' 부호가 연속되어 나타나는 경우 ex) ----3
				int increment = unaryChecker(postfix,operator,infix,index);
				if(increment!=0)
					adder = increment;
			}
			//에러처리 : 잘못된 input(whitespace/digit/operator 모두 아닌경우)
			else{
				throw new Exception();
			}
		}

		//STEP2: operator에 남은 요소들은 stack에 집어넣기
		while(!operator.isEmpty()){
			String restOP = operator.remove();
			postfix.push(restOP);
		}
		//에러처리 : '('와 ')' 갯수 불일치
		if(postfix.existBracket())
			throw new Exception();
		//에러처리 : 연산자가 피연산자에 비해 지나치게 많은 경우
		if(postfix.toomuchOperator())
			throw new Exception();
		return postfix;
	}
	
	//input operator와 stack에 있는 operator들을 비교하여 적절히 push
	private static void shiftingOperator(Stack postfix, Stack operator,String OP){
		while(!operator.isEmpty()){
			String BEFOREOP = operator.pop();
			char beforeOP = BEFOREOP.charAt(0);
			//CASE1 : input operator의 priority가 이전 operator에 비해 priority가 높을 경우
			//예외처리 : Stack에서 뽑힌 operator가 '('이거나, ---방식으로 부호가 연속해서 들어오거나, ^^방식으로 ^부호가 연속해서 들어올 때 그냥 push
			if(compareTo(OP,BEFOREOP)==1 || compareTo(OP,BEFOREOP)==2|| beforeOP=='('){
				operator.push(OP);
				break;
			}
			//CASE2 : input operator의 priority가 이전 operator에 비해 priority가 작거나 같을 때 
			else{
				operator.remove();
				postfix.push(BEFOREOP);
				//예외처리 : 더 이상 operator stack에 남은 operator가 없는 경우(input operator를 push)
				if(operator.isEmpty()){
					operator.push(OP);
					break;
				}
			}	
		}
	}
	
	//부호가 존재하는지 확인하는 method
	private static int unaryChecker(Stack postfix, Stack operator, String infix, int index) throws Exception{
		//STEP1 : operator 뒤이어 부호 '-' 나올 시 '~'로 변형 후 push
		//STEP2 : 부호 '-'가 연속해서 나오는게 종료 시, 앞선 String 탐색 위치를 변경시킴
		int increment = 0;
		int counter=increment;
		do{
			int unaryIndex = checkUnary(infix,index+increment);
			//CASE1 : 부호 이외에 operator가 나오거나 operand가 나올 시 break
			if(unaryIndex==0){
				increment+=1;
				break;
			}
			//CASE2 : operator로 부호 - 출현 시 stack에 push
			else{
				char sign = '~';
				String unary = Character.toString(sign);
				shiftingOperator(postfix,operator,unary);
				counter = unaryIndex;
				increment= counter-index;
			}
			
		}while(true);
		
		return increment;
	}
	
	//operator뒤에 연이어 '-'가 나오는지 체크
	private static int checkUnary(String infix,int index) throws Exception{
		for(int i=index+1;i<infix.length();i++){
			char isItUnary = infix.charAt(i);
			//CASE1 : whitespace
			if(isItUnary== ' ')
				continue;
			//CASE2 : operand
			else if(!isOperator(isItUnary)){
				return 0;
			}
			//CASE3 : 부호 '-' 발견
			else if(isItUnary == '-'){
				return i;
			}
			//예외처리 : 잘못된 input
			else{
				return 0;
			}
		}
		return 0;
	}

	
	//digit의 크기 파악 method
	private static int checkDigit(String infix, int index){
		char substring = infix.charAt(index);
		if(Character.isDigit(substring)){
			if(index==infix.length()-1)
				return 1;
			else
				return 1 + checkDigit(infix,index+1);
		}
		else{
			return 0;
		}
	}
	
	//operator 확인
	private static boolean isOperator(char substring) throws Exception{
		if(substring=='(' || substring==')' || substring == '^' || substring == '-' || substring == '*' || substring == '/' || substring== '%' || substring=='+' || substring=='~')
			return true;
		else if(Character.isDigit(substring)){
			return false;
		}
		else
			throw new Exception();
	}

	//두 string 간의 우선순위 비교 후 stack에 push/pop
	private static int compareTo(String operator, String popItem){
		//예외처리 : '-'와 '~'가 연속으로 들어왔을 때, push
		if((operator.equals(String.valueOf('~')) && popItem.equals(String.valueOf('~'))) || (operator.equals(String.valueOf('~')) && popItem.equals(String.valueOf('-'))))
			return 2;
		//예외처리 : '^'와 '^'가 연속으로 들어왔을 때, push
		else if((operator.equals(String.valueOf('^')) && popItem.equals(String.valueOf('^'))))
			return 2;
		//input operator와 stack에 있던 operator보다 우선순위가 높으면, push
		else if(priority(operator)>priority(popItem))
			return 1;
		//input operator와 stack에 있던 operator가 동일 시, pop
		else if(priority(operator)==priority(popItem))
			return 0;
		else
			return -1;
	}
	
	//연산자 간의 우선순위를 정해주는 method
	private static int priority(String item){
		char operator = item.charAt(0);
		if(operator=='(' || operator==')')
			return 5;
		else if(operator=='^')
			return 4;
		else if(operator=='~')
			return 3;
		else if(operator=='*' || operator=='/' || operator =='%')
			return 2;
		else if(operator=='+' || operator == '-')
			return 1;
		else
			throw new NoSuchElementException();
			
	}
	
	//postfix를 계산하는 method
	private static String calculation(Stack postfix) throws Exception{
		Stack result = new Stack();
		postfix.atLast();
		//STEP1 : postfix stack에서 operand/operator를 구별하여 계산
		while(true){
			String newly = postfix.pointer.item;
			char item = newly.charAt(0);
			//CASE1 : operand는 stack에 push
			if(!isOperator(item)){
				result.push(newly);
			}
			//CASE2 : '~' operator는 operand를 -값으로 변경
			else if(isOperator(item) && item=='~'){
				String firstItem = result.remove();
				long firstNum = Long.parseLong(firstItem);
				firstNum = 0 - firstNum;
				String afterCalculation = String.valueOf(firstNum);
				result.push(afterCalculation);
			}
			//CASE3 : 기타 operator들은 앞선 두 operand 계산
			else{
				String firstItem = result.remove();
				String secondItem = result.remove();
				long firstNum = Long.parseLong(firstItem);
				long secondNum = Long.parseLong(secondItem);
				String afterCalculation = calculate(item,firstNum,secondNum);
				result.push(afterCalculation);
			}
			
			if(postfix.pointer==postfix.top)
				break;
			postfix.pointer=postfix.pointer.prev;
		}
		//STEP2 : STACK에 남은 최종 결과값 return
		return result.remove();
	}
	
	private static String calculate(char item, long firstNum, long secondNum) throws Exception{
		long result;
		if(item=='+')
			result = secondNum + firstNum;
		else if(item=='-')
			result = secondNum - firstNum;
		else if(item=='*')
			result = secondNum * firstNum;
		else if(item=='/'){
			//에러처리 : 0으로 나누는 경우
			if(firstNum==0)
				throw new Exception();
			result = secondNum / firstNum;
			
		}
		else if(item=='%'){
			//에러처리 : 0으로 나누는 경우
			if(firstNum==0)
				throw new Exception();
			result = secondNum % firstNum;
		}
		else if(item=='^'){
			//에러처리 : 0에 음수를 제곱하는 경우
			if(firstNum<0 && secondNum==0)
				throw new Exception();
			result = (long)Math.pow((long)secondNum, (long)firstNum);
		}
			 
		else
			throw new NoSuchElementException();
		String str = String.valueOf(result);
		return str;
	}
}